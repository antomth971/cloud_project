import util from 'util';
import express from 'express';
import bodyParser from 'body-parser';
import { DefaultAzureCredential } from '@azure/identity';
import { ComputeManagementClient } from '@azure/arm-compute';
import { ResourceManagementClient } from '@azure/arm-resources';
import { NetworkManagementClient } from '@azure/arm-network';
import { StorageManagementClient } from "@azure/arm-storage";
import dotenv from "dotenv";
import cors from 'cors';
import { Worker } from 'worker_threads';

dotenv.config();

let randomIds = {};
let subnetInfo = null;
let publicIPInfo = null;
let vmImageInfo = null;
let nicInfo = null;

const vmName = _generateRandomId("testvm", randomIds);
const storageAccountName = _generateRandomId("testac", randomIds);
const vnetName = _generateRandomId("testvnet", randomIds);
const subnetName = _generateRandomId("testsubnet", randomIds);
const publicIPName = _generateRandomId("testpip", randomIds);
const networkInterfaceName = _generateRandomId("testnic", randomIds);
const ipConfigName = _generateRandomId("testcrpip", randomIds);
const domainNameLabel = _generateRandomId("testdomainname", randomIds);
const osDiskName = _generateRandomId("testosdisk", randomIds);

const resourceGroupName = "test-resource-group"
const location = "eastus";
const accType = "Standard_LRS";
let publisher = "Canonical";
let offer = "UbuntuServer";
let sku = "14.04.3-LTS";
const adminUsername = "notadmin";
const adminPassword = "azertyAZE92";


const clientId = process.env["AZURE_CLIENT_ID"];
const domain = process.env["AZURE_TENANT_ID"];
const secret = process.env["AZURE_CLIENT_SECRET"];
const subscriptionId = process.env["AZURE_SUBSCRIPTION_ID"];


const app = express();
app.use(bodyParser.json());
app.use(cors());

if (!clientId || !domain || !secret || !subscriptionId) {
    console.log("Default credentials couldn't be found");
  }
const credentials = new DefaultAzureCredential();
const resourceClient = new ResourceManagementClient(credentials, subscriptionId);
const computeClient = new ComputeManagementClient(credentials, subscriptionId);
const storageClient = new StorageManagementClient(credentials, subscriptionId);
const networkClient = new NetworkManagementClient(credentials, subscriptionId);

app.post('/startVM', async (req, res) => {
    try {
        switch (req.body.machine) {
            case "unbuntu":
                publisher = "Canonical";
                offer = "UbuntuServer";
                sku = "14.04.3-LTS";
                break;
            case "windows":
                publisher = "MicrosoftWindowsServer";
                offer = "WindowsServer";
                sku = "2019-Datacenter";    
                break;
            case "debian":
                publisher = "Debian";
                offer = "debian-10";
                sku = "10";
                break;
        }
        await createResources();
        await manageResources();
    
        const ip = await getIp();
        const tes = await getVirtualMachines();
        console.log("-------------------");
        console.log(tes);
        const worker = new Worker('./workerDeleteVM.js', { workerData: null });

        res.json({ success: true, message: 'La machine virtuelle a été créée avec succès.', ipAdress: ip.ipAddress,id:adminUsername,password : adminPassword });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Une erreur s\'est produite lors de la création de la machine virtuelle.' });
    }
});

app.delete('/destroyVM', async (req, res) => {
    try {
        await deleteResourceGroup()
        res.json({ success: true, message: 'La machine virtuelle a été détruite avec succès.' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Une erreur s\'est produite lors de la destruction de la machine virtuelle.' });
    }
});


const createResources = async () => {
    try {
        const result = await createResourceGroup();
        const accountInfo = await createStorageAccount();
        const vnetInfo = await createVnet();
        subnetInfo = await getSubnetInfo();
        publicIPInfo = await createPublicIP();
        nicInfo = await createNIC(subnetInfo, publicIPInfo);
        vmImageInfo = await findVMImage();
        const nicResult = await getNICInfo();
        const vmInfo = await createVirtualMachine(nicInfo.id, vmImageInfo[0].name);
        return;
    } catch (err) {
        console.log(err);
    }
};

const createResourceGroup = async () => {
    const groupParameters = {
        location: location,
        tags: { sampletag: "sampleValue" },
    };
    console.log("\n1.Creating resource group: " + resourceGroupName);
    return await resourceClient.resourceGroups.createOrUpdate(
        resourceGroupName,
        groupParameters
    );
};

const createStorageAccount = async () => {
    console.log("\n2.Creating storage account: " + storageAccountName);
    const createParameters = {
        location: location,
        sku: {
            name: accType,
        },
        kind: "Storage",
        tags: {
            tag1: "val1",
            tag2: "val2",
        },
    };
    return await storageClient.storageAccounts.beginCreateAndWait(
        resourceGroupName,
        storageAccountName,
        createParameters
    );
};

const createVnet = async () => {
    const vnetParameters = {
        location: location,
        addressSpace: {
            addressPrefixes: ['10.0.0.0/16'],
        },
        dhcpOptions: {
            dnsServers: ['10.1.1.1', '10.1.2.4'],
        },
        subnets: [{ name: subnetName, addressPrefix: '10.0.0.0/24' }],
    };
    console.log('\n3.Creating vnet: ' + vnetName);
    return await networkClient.virtualNetworks.beginCreateOrUpdateAndWait(
        resourceGroupName,
        vnetName,
        vnetParameters
    );
};

const getSubnetInfo = async () => {
    console.log('\nGetting subnet info for: ' + subnetName);
    return await networkClient.subnets.get(resourceGroupName, vnetName, subnetName);
};

const createPublicIP = async () => {
    const publicIPParameters = {
        location: location,
        publicIPAllocationMethod: 'Dynamic',
        dnsSettings: {
            domainNameLabel: domainNameLabel,
        },
    };
    console.log('\n4.Creating public IP: ' + publicIPName);
    return await networkClient.publicIPAddresses.beginCreateOrUpdateAndWait(
        resourceGroupName,
        publicIPName,
        publicIPParameters
    );
};

const createNIC = async (subnetInfo, publicIPInfo) => {
    const nicParameters = {
        location: location,
        ipConfigurations: [
            {
                name: ipConfigName,
                privateIPAllocationMethod: 'Dynamic',
                subnet: subnetInfo,
                publicIPAddress: publicIPInfo,
            },
        ],
    };
    console.log('\n5.Creating Network Interface: ' + networkInterfaceName);
    return await networkClient.networkInterfaces.beginCreateOrUpdateAndWait(
        resourceGroupName,
        networkInterfaceName,
        nicParameters
    );
};

const findVMImage = async () => {
    return await computeClient.virtualMachineImages.list(location, publisher, offer, sku, { top: 1 });
};

const getNICInfo = async () => {
    return await networkClient.networkInterfaces.get(resourceGroupName, networkInterfaceName);
};
const getIp = async () => {
    return await networkClient.publicIPAddresses.get(resourceGroupName, publicIPName);
}

const createVirtualMachine = async (nicId, vmImageVersionNumber) => {
    const vmParameters = {
        location: location,
        osProfile: {
            computerName: vmName,
            adminUsername: adminUsername,
            adminPassword: adminPassword,
        },
        hardwareProfile: {
            vmSize: 'Standard_B1ls',
        },
        storageProfile: {
            imageReference: {
                publisher: publisher,
                offer: offer,
                sku: sku,
                version: vmImageVersionNumber,
            },
            osDisk: {
                name: osDiskName,
                caching: 'None',
                createOption: 'fromImage',
                vhd: { uri: 'https://' + storageAccountName + '.blob.core.windows.net/nodejscontainer/osnodejslinux.vhd' },
            },
        },
        networkProfile: {
            networkInterfaces: [
                {
                    id: nicId,
                    primary: true,
                },
            ],
        },
    };
    console.log('6.Creating Virtual Machine: ' + vmName);
    console.log(' VM create parameters: ' + util.inspect(vmParameters, { depth: null }));
    await computeClient.virtualMachines.beginCreateOrUpdateAndWait(resourceGroupName, vmName, vmParameters);
};

const manageResources = async () => {
    await getVirtualMachines();
    await startVirtualMachines(resourceGroupName, vmName);
    const resultListVirtualMachines = await listVirtualMachines();
    console.log(
        util.format(
            'List all the vms under the current ' +
            'subscription \n%s',
            util.inspect(resultListVirtualMachines, { depth: null })
        )
    );
};

const getVirtualMachines = async () => {
    console.log(`Get VM Info about ${vmName}`);
    return await computeClient.virtualMachines.get(resourceGroupName, vmName);
};

const startVirtualMachines = async () => {
    console.log(`Start the VM ${vmName}`);
    return await computeClient.virtualMachines.beginStart(resourceGroupName, vmName);
};

const listVirtualMachines = async () => {
    console.log(`Lists VMs`);
    const result = new Array();
    for await (const item of computeClient.virtualMachines.listAll()) {
        result.push(item);
    }
    return result;
};

function _generateRandomId(prefix, existIds) {
    var newNumber;
    while (true) {
        newNumber = prefix + Math.floor(Math.random() * 10000);
        if (!existIds || !(newNumber in existIds)) {
            break;
        }
    }
    return newNumber;
}

const deleteResourceGroup = async () => {
    console.log("\nDeleting resource group: " + resourceGroupName);
    return await resourceClient.resourceGroups.beginDeleteAndWait(resourceGroupName);
};

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Serveur Express écoutant sur le port ${port}`);
});
