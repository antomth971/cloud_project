# Projet cloud

Projet réalisé par MATHIEU Anthony.

## Sommaire

- [Projet cloud](#projet-cloud)
  - [Sommaire](#sommaire)
  - [Description](#description)
    - [Sujet](#sujet)
    - [Technologies utilisées](#technologies-utilisées)
  - [Installation de l'environnement backend et des libraries](#installation-de-lenvironnement-backend-et-des-libraries)
    - [Fichier `.env`](#fichier-env)
    - [Commandes](#commandes)
    - [Explications](#explications)
  - [Interface Web](#interface-web)
    - [Formulaire de connexion :](#formulaire-de-connexion-)
    - [page utilisateur sans machine :](#page-utilisateur-sans-machine-)
    - [page utilisateur avec une machine :](#page-utilisateur-avec-une-machine-)
    - [page utilisateur avec plusieurs machines :](#page-utilisateur-avec-plusieurs-machines-)
    - [Identifiants](#identifiants)
  - [Connexion à une machine distante](#connexion-à-une-machine-distante)
    - [Windows](#windows)
    - [Linux (unbuntu et debian)](#linux-unbuntu-et-debian)
  - [Lancement du projet](#lancement-du-projet)
    - [Backend](#backend)
    - [Frontend](#frontend)
    - [worker](#worker)
    - [scripts en plus](#scripts-en-plus)

## Description

### Sujet

Un site web permettant à un développeur d’avoir facilement une plateforme
pour tester ses programmes.
- Le site web doit attendre une authentification:
- Prévoyez 3 utilisateurs déjà configurés
- Les logins et mots de passe des utilisateurs sont mentionnés dans la
documentation
- Un utilisateur ne doit avoir aucun crédit, donc ne peut rien faire
- Un utilisateur doit avoir accès à une seule machine préconfigurée
- Un utilisateur doit avoir accès à plusieurs systèmes d’exploitations et il peut choisir lequel il démarre.
- Quand un utilisateur enregistré lance une machine, le site web la crée sur le portail Azure, et indique à l’utilisateur les paramètres de connexion (RDP et/ou SSH)
- L’utilisateur a 10 minutes pour se connecter sur sa machine avant qu’elle soit détruite automatiquement.
- En bonus, il doit être possible de prévoir des machines linux sur lesquelles on peut se connecter en mode graphique (peut être avec le protocole VNC)
- Il n’y a pas de pré-requis en terme de langage. Vous pouvez utiliser le langage ou le framework de votre choix.
- Le code source doit être documenté.

### Technologies utilisées

- Node.js (express) pour le backend
- vanilla JS, HTML, CSS pour le frontend

## Installation de l'environnement backend et des libraries

### Fichier `.env`

```env	
AZURE_TENANT_ID=b7b023b8-7c32-4c02-92a6-c8cdaa1d189c
AZURE_CLIENT_SECRET="f6y8Q~rHOJlOLrIouAc30zHHIbRIduRIRPhQXa3t"
AZURE_SUBSCRIPTION_ID=3df72bda-22d8-49bf-a135-de221bffc065
AZURE_CLIENT_ID=5f99df3b-dadb-4a15-97e4-433e237c2103
```
Remplacer les valeurs par les identifiants de votre compte Azure. Nous utiliserons ensuite la librairie `dotenv` pour charger ces variables d'environnement dans `process.env`

### Commandes 

```bash

sudo apt install npm
npm i dotenv
npm install express @azure/identity @azure/arm-compute 
npm install cors
sudo apt-get install rdesktop
sudo apt-get install freerdp2-x11
npm i 

```

(mettre à jour les paquets avec `sudo apt-get update` avant d'installer les paquets)

Nous prendrons également une version supérieur à 20 pour node.js.

```bash

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
nvm install 20
nvm use 20
nvm alias default 20

```
### Explications 

- `npm idotenv` : Permet de charger les variables d'environnement depuis un fichier `.env` dans `process.env`
- `npm install express @azure/identity @azure/arm-compute` : Permet d'installer les librairies nécessaires pour le backend avec AZURE
- `npm i` : Permet d'installer les dépendances du projet
- `npm install cors` : Permet d'installer la librairie `cors` pour gérer les requêtes HTTP
- `sudo apt-get install rdesktop` : Permet d'installer le client RDP
- `sudo apt-get install freerdp2-x11` : Permet d'installer le client RDP

Ps : Pour se connecter à une machine Windows depuis un terminal Linux, on peut utiliser la commande `rdesktop` ou `xfreerdp`

## Interface Web

### Formulaire de connexion :

![Interface Web](img/formulaire.png)

### page utilisateur sans machine :

![utilisateur sans machine](img/pauvre.png)

### page utilisateur avec une machine :

![utilisateur avec une seule machine](img/normal.png)

### page utilisateur avec plusieurs machines :

![utilisateur plusieurs machines](img/riche.png)


### Identifiants

Ci-dessous les différents identifiants pour se connecter aux différents profils :


| Id | Mot de passe | Role |
|-----------|-----------|-----------|
| John | password123 | Pas de machine |
| Alice | qwerty | une machine (linux : unbuntu) |
| Bob | abc123 | plusieurs machine |

## Connexion à une machine distante

### Windows

Dans le cas d'une machine Windows, on utilise la commande `xfreerdp` pour se connecter à la machine distante.

```bash
xfreerdp /v:ADRESSE_IP /u:notadmin /p:azertyAZE92
```
Remplacer `ADRESSE_IP` par l'adresse IP de la machine distante.

### Linux (unbuntu et debian)

Dans le cas d'une machine Linux, on utilise la commande `ssh` pour se connecter à la machine distante.

```bash
ssh -o PasswordAuthentication=yes notadmin@ADRESSE_IP
```

Remplacer `ADRESSE_IP` par l'adresse IP de la machine distante.

## Lancement du projet

### Backend

```bash

node backend.js

```

Lancer cette commande depuis la racine du projet, elle permet de lancer le serveur backend sur le port 3000.

### Frontend

Ouvrir le fichier `index.html` dans un navigateur web.

### worker

Il y a un script qui permet de supprimer un groupe de ressource après un certain temps (10 minutes) dans le fichier `workerDeleteVM.js`. Il est appelé avant le retour de la fonction `createVM` dans le fichier `backend.js`.

```js
import dotenv from "dotenv";
import { ResourceManagementClient } from '@azure/arm-resources';
import { DefaultAzureCredential } from '@azure/identity';

dotenv.config();

const credentials = new DefaultAzureCredential();
const subscriptionId = process.env["AZURE_SUBSCRIPTION_ID"];

const resourceClient = new ResourceManagementClient(credentials, subscriptionId);
const resourceGroupName = "test-resource-group"

setTimeout(async () => {
        await deleteResourceGroup();
        console.log("Resource group deleted");
}, 10 * 60 * 1000);

const deleteResourceGroup = async () => {
    console.log("\nDeleting resource group: " + resourceGroupName);
    return await resourceClient.resourceGroups.beginDeleteAndWait(resourceGroupName);
};
```

### scripts en plus

```bash	

node index.js

```

Elle permet de créer une vm random sur le portail azure.

```bash

node cleanup.js NOM_GROUPE_RESSOURCE

```	

Remplacer `NOM_GROUPE_RESSOURCE` par le nom du groupe de ressource à nettoyer (supprimer toutes les machines virtuelles et les disques associés), ici `test-resource-group`.