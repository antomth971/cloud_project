import dotenv from "dotenv";
import { ResourceManagementClient } from '@azure/arm-resources';
import { DefaultAzureCredential } from '@azure/identity';

dotenv.config();

const credentials = new DefaultAzureCredential();
const subscriptionId = process.env["AZURE_SUBSCRIPTION_ID"];

const resourceClient = new ResourceManagementClient(credentials, subscriptionId);
const resourceGroupName = "test-resource-group"

setTimeout(async () => {
        await deleteResourceGroup();
        console.log("Resource group deleted");
}, 10 * 60 * 1000);

const deleteResourceGroup = async () => {
    console.log("\nDeleting resource group: " + resourceGroupName);
    return await resourceClient.resourceGroups.beginDeleteAndWait(resourceGroupName);
};
