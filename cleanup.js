import { DefaultAzureCredential } from "@azure/identity";
import { ResourceManagementClient } from "@azure/arm-resources";
import dotenv from "dotenv";
dotenv.config();

const clientId = process.env["AZURE_CLIENT_ID"];
const domain = process.env["AZURE_TENANT_ID"];
const secret = process.env["AZURE_CLIENT_SECRET"];
const subscriptionId = process.env["AZURE_SUBSCRIPTION_ID"];


if (!clientId || !domain || !secret || !subscriptionId) {
  console.log("Default credentials couldn't be found");
}

const resourceGroupName = process.argv[2];
if (!resourceGroupName) {
  console.log("resourceGroupName couldn't be found");
}

const credentials = new DefaultAzureCredential();
const resourceClient = new ResourceManagementClient(credentials, subscriptionId);

const deleteResourceGroup = async () => {
  console.log("\nDeleting resource group: " + resourceGroupName);
  return await resourceClient.resourceGroups.beginDeleteAndWait(resourceGroupName);
};

const main = async () => {
  try {
    console.log(
      "Deleting the resource group can take a few minutes, so please be patient :)."
    );

    await deleteResourceGroup();
    console.log("Successfully deleted the resource group: " + resourceGroupName);
  } catch (err) {
    console.log(err);
  }
};

main()
  .then(() => {
    console.log("success");
  })
  .catch((err) => {
    console.log(err);
  });
