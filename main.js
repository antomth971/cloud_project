const divLogin = document.querySelector("#divLogin");
const username = document.querySelector("[name='username']");
const password = document.querySelector("[name='password']");
const btnLogin = document.querySelector("#login");
const divUser1 = document.querySelector("#divUser1");
const divUser2 = document.querySelector("#divUser2");
const divUser3 = document.querySelector("#divUser3");
const uneVM = document.querySelector("#uneVM");
const deleteVMInput = document.querySelector("#deleteVM");
const plusieursVM = document.querySelector("#plusieursVM");

const cheminVersFichier = 'User.json';

let donneesJSON;

fetch(cheminVersFichier)
    .then(response => response.json())
    .then(data => {
        donneesJSON = data;

    })
    .catch(err => console.error('Erreur de récupération du fichier JSON :', err));


function submitLogin() {
    if (username.value != "" && password.value != "") {
        const user = donneesJSON.Users.find((user) => {
            if (user.name === username.value)
                return user
        });
        if (user && user.password === password.value) {
            divLogin.style.display = "none";
            switch (user.roleId) {
                case 1:
                    divUser1.removeAttribute("style");
                    break;
                case 2:
                    divUser2.removeAttribute("style");
                    break;
                case 3:
                    divUser3.removeAttribute("style");
                    break;
            }
        } else {
            alert('Invalid email or password');
        }
    } else {
        alert("complete username and password");
    }
}

function logout() {
    divUser1.style.display = "none";
    divUser2.style.display = "none";
    divUser3.style.display = "none";
    divLogin.removeAttribute("style");
}


const createVM = async (machine) => {
    createLoader();
    uneVM.setAttribute("style", "display: none;");
    plusieursVM.setAttribute("style", "display: none;");
    let machineName = "";
    switch (machine) {
        case 1:
            machineName = "unbuntu";
            break;
        case 2:
            machineName = "windows";
            break;
        case 3:
            machineName = "debian";
            break;
    }
    try {
        const response = await fetch('http://localhost:3000/startVM', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                machine:machineName
            }),
        });

        if (!response.ok) {
            throw new Error('Erreur lors de la création de la machine virtuelle');
        }

        const result = await response.json();
        let loader = document.querySelector("#loader");
        loader.remove();
        alert("La machine virtuelle a été créée avec succès\n id : " + result.id + "\n password : " + result.password + "\n ip adress : " + result.ipAdress);
    } catch (error) {
        console.error(error.message);
        let loader = document.querySelector("#loader");
        loader.remove();
    }
};


const deleteVM = async () => {
    createLoader();
    try {
        const response = await fetch('http://localhost:3000/destroyVM', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (!response.ok) {
            throw new Error('Erreur lors de la suppression de la machine virtuelle');
        }

        const result = await response.json();
        console.log(result.message);
        let loader = document.querySelector("#loader");
        loader.remove();
        plusieursVM.removeAttribute("style");
        uneVM.removeAttribute("style");
    } catch (error) {
        console.error(error.message);
    }
};

function lunchVM(machine) {
    createVM(machine);
}
function createLoader() {
    let loader = document.createElement("div");
    loader.id = "loader";
    loader.style.backgroundColor = "white";
    loader.style.position = "fixed";
    loader.style.top = "50%";
    loader.style.left = "50%";
    loader.style.height = "100%"
    loader.style.width = "100%"
    loader.style.transform = "translate(-50%, -50%)";
    loader.innerHTML = `<div class='d-flex justify-content-center'><div><h1>En cours de chargement</h1><div></div><span class="loader"></span></div></div>`;
    document.body.appendChild(loader);
}